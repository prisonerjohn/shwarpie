﻿using System.Collections.Generic;
using UnityEngine;

namespace Shwarpie
{
	public class VertexData
	{
		public Vector2 Coord { get; set; }
		public Vector3 Pos { get; set; }
		public Vector2 Uv { get; set; }
	}

	public class GridWarpData
	{
		public int NumCols { get; set; }
		public int NumRows { get; set; }

		public List<VertexData> Vertices { get; set; } = new List<VertexData>();
	}
}