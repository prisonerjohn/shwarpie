﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Shwarpie
{
	[Serializable]
	public class WarpVertex
	{
		public Vector2 coord;
		public Vector3 pos;
		public Vector2 uv;
		public GameObject go;
	}

	[ExecuteInEditMode]
	[RequireComponent(typeof(MeshFilter))]
	public class GridWarp
		: MonoBehaviour
	{
		public string dataFile = "GridWarp.json";
		public GameObject handlePrefab;

		public KeyCode editToggleKey = KeyCode.G;

		public float translateStepAmount = 0.01f;
		public float translateNudgeAmount = 0.001f;

		public float rotateStepAmount = 0.01f;
		public float rotateNudgeAmount = 0.001f;

		public float scaleStepAmount = 0.01f;
		public float scaleNudgeAmount = 0.001f;

		private int _currHandleIdx = 0;
		private bool[] _editHandleFlags = null;

		[SerializeField]
		private int _numCols;
		public int NumCols
		{
			get { return _numCols; }
			set { _numCols = Mathf.Max(2, value); }
		}

		[SerializeField]
		private int _numRows;
		public int NumRows
		{
			get { return _numRows; }
			set { _numRows = Mathf.Max(2, value); }
		}

		[SerializeField]
		private List<WarpVertex> _vertices = new List<WarpVertex>();
		public List<WarpVertex> Vertices
		{
			get { return _vertices; }
			private set { _vertices = value; }
		}

		private bool _resetVertices;

		private bool _editing;
		public bool Editing
		{
			get 
			{ 
				return _editing; 
			}
			set
			{
				if (_editing == value) return;
				_editing = value;

				if (_editing)
				{
					// Add handles for all vertices.
					for (int i = 0; i < _vertices.Count; ++i)
					{
						if (_vertices[i].go == null)
						{
							_vertices[i].go = Instantiate(handlePrefab);
						}
						_vertices[i].go.layer = gameObject.layer;
						_vertices[i].go.name = string.Format("Vertex [{0:0}, {1:0}]", _vertices[i].coord.x, _vertices[i].coord.y);
						_vertices[i].go.transform.parent = transform;
						_vertices[i].go.transform.localPosition = _vertices[i].pos;
					}

					_currHandleIdx = 0;
					_editHandleFlags = new bool[_vertices.Count];
				}
				else
				{
					// Remove all the vertex handles.
					for (int i = 0; i < _vertices.Count; ++i)
					{
						if (_vertices[i].go != null)
						{
#if UNITY_EDITOR
							DestroyImmediate(_vertices[i].go);
#else
							Destroy(_vertices[i].go);
#endif
						}
						_vertices[i].go = null;
					}
				}
			}
		}

		private MeshFilter _meshFilter;

		public void Awake()
		{
			_meshFilter = GetComponent<MeshFilter>();
		}

		public void Start()
		{ 
			if (!string.IsNullOrEmpty(dataFile))
			{
				LoadData();
			}
		}

		public void Update()
		{
			if ((Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)) && Input.GetKeyUp(editToggleKey))
			{
				Editing = !Editing;
			}

			if (Editing)
			{
				HandleEditInput();
				UpdateHandles();
				RebuildMesh();
			}
		}

		public void ResetVertices()
		{
			_vertices.Clear();
			while (transform.childCount > 0)
			{
#if UNITY_EDITOR
				DestroyImmediate(transform.GetChild(0).gameObject);
#else
				Destroy(transform.GetChild(0).gameObject);
#endif
			}

			_resetVertices = true;
		}

		public void HandleEditInput()
		{
			if ((Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl)) && Input.GetKeyDown(KeyCode.S))
			{
				SaveData();
			}
			else if ((Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl)) && Input.GetKeyDown(KeyCode.L))
			{
				LoadData();
			}

			if (Input.GetKeyDown(KeyCode.PageUp) || Input.GetKeyDown(KeyCode.Tab))
			{
				if (!Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.RightShift))
				{
					// Clear all previous handles.
					for (int i = 0; i < _editHandleFlags.Length; ++i)
					{
						_editHandleFlags[i] = false;
					}
				}

				// Enable next handle flag.
				_currHandleIdx = (_currHandleIdx + 1) % Vertices.Count;
				_editHandleFlags[_currHandleIdx] = true;
			}
			else if (Input.GetKeyDown(KeyCode.PageDown) || Input.GetKeyDown(KeyCode.Backspace))
			{
				if (!Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.RightShift))
				{
					// Clear all previous handles.
					for (int i = 0; i < _editHandleFlags.Length; ++i)
					{
						_editHandleFlags[i] = false;
					}
				}

				// Enable prev handle flag.
				_currHandleIdx -= 1;
				while (_currHandleIdx < 0)
				{
					_currHandleIdx += Vertices.Count;
				}
				_editHandleFlags[_currHandleIdx] = true;
			}
			else if (Input.GetKeyDown(KeyCode.Delete))
			{
				// Clear all previous handles.
				for (int i = 0; i < _editHandleFlags.Length; ++i)
				{
					_editHandleFlags[i] = false;
				}
			}

			if (Input.GetKey(KeyCode.W))
			{
				// Translate!
				var translateScalar = (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)) ? translateNudgeAmount : translateStepAmount;
				if (Input.GetKeyDown(KeyCode.LeftArrow))
				{
					var offsetPos = new Vector3(-translateScalar, 0, 0);
					for (int i = 0; i < _editHandleFlags.Length; ++i)
					{
						if (_editHandleFlags[i])
						{
							_vertices[i].go.transform.localPosition += offsetPos;
						}
					}
				}
				else if (Input.GetKeyDown(KeyCode.RightArrow))
				{
					var offsetPos = new Vector3(translateScalar, 0, 0);
					for (int i = 0; i < _editHandleFlags.Length; ++i)
					{
						if (_editHandleFlags[i])
						{
							_vertices[i].go.transform.localPosition += offsetPos;
						}
					}
				}
				else if (Input.GetKeyDown(KeyCode.DownArrow))
				{
					var offsetPos = new Vector3(0, 0, -translateScalar);
					for (int i = 0; i < _editHandleFlags.Length; ++i)
					{
						if (_editHandleFlags[i])
						{
							_vertices[i].go.transform.localPosition += offsetPos;
						}
					}
				}
				else if (Input.GetKeyDown(KeyCode.UpArrow))
				{
					var offsetPos = new Vector3(0, 0, translateScalar);
					for (int i = 0; i < _editHandleFlags.Length; ++i)
					{
						if (_editHandleFlags[i])
						{
							_vertices[i].go.transform.localPosition += offsetPos;
						}
					}
				}
			}
			else if (Input.GetKey(KeyCode.E))
			{
				// Rotate!
				var rotateScalar = (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)) ? rotateNudgeAmount : rotateStepAmount;
				var rotateCenter = Vector3.zero;
				var rotateCount = 0;
				for (int i = 0; i < _editHandleFlags.Length; ++i)
				{
					if (_editHandleFlags[i])
					{
						rotateCenter += _vertices[i].go.transform.localPosition;
						++rotateCount;
					}
				}
				if (rotateCount > 0)
				{
					rotateCenter /= rotateCount;
				}
		
				if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.DownArrow))
				{
					for (int i = 0; i < _editHandleFlags.Length; ++i)
					{
						if (_editHandleFlags[i])
						{
							_vertices[i].go.transform.localPosition -= rotateCenter;
							float radius = Mathf.Sqrt(_vertices[i].go.transform.localPosition.x * _vertices[i].go.transform.localPosition.x + _vertices[i].go.transform.localPosition.z * _vertices[i].go.transform.localPosition.z);
							float angle = Mathf.Atan2(_vertices[i].go.transform.localPosition.z, _vertices[i].go.transform.localPosition.x);
							angle += rotateScalar;
							_vertices[i].go.transform.localPosition = new Vector3(radius * Mathf.Cos(angle), 0.0f, radius * Mathf.Sin(angle));
							_vertices[i].go.transform.localPosition += rotateCenter;
						}
					}
				}
				else if (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.UpArrow))
				{
					for (int i = 0; i < _editHandleFlags.Length; ++i)
					{
						if (_editHandleFlags[i])
						{
							_vertices[i].go.transform.localPosition -= rotateCenter;
							float radius = Mathf.Sqrt(_vertices[i].go.transform.localPosition.x * _vertices[i].go.transform.localPosition.x + _vertices[i].go.transform.localPosition.z * _vertices[i].go.transform.localPosition.z);
							float angle = Mathf.Atan2(_vertices[i].go.transform.localPosition.z, _vertices[i].go.transform.localPosition.x);
							angle -= rotateScalar;
							_vertices[i].go.transform.localPosition = new Vector3(radius * Mathf.Cos(angle), 0.0f, radius * Mathf.Sin(angle));
							_vertices[i].go.transform.localPosition += rotateCenter;
						}
					}
				}
			}
			else if (Input.GetKey(KeyCode.R))
			{
				// Scale!
				var scaleScalar = (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)) ? scaleNudgeAmount : scaleStepAmount;
				var scaleCenter = Vector3.zero;
				var scaleCount = 0;
				for (int i = 0; i < _editHandleFlags.Length; ++i)
				{
					if (_editHandleFlags[i])
					{
						scaleCenter += _vertices[i].go.transform.localPosition;
						++scaleCount;
					}
				}
				if (scaleCount > 0)
				{
					scaleCenter /= scaleCount;
				}

				if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.DownArrow))
				{
					scaleScalar = 1.0f - scaleScalar;
					for (int i = 0; i < _editHandleFlags.Length; ++i)
					{
						if (_editHandleFlags[i])
						{
							_vertices[i].go.transform.localPosition -= scaleCenter;
							_vertices[i].go.transform.localPosition *= scaleScalar;
							_vertices[i].go.transform.localPosition += scaleCenter;
						}
					}
				}
				else if (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.UpArrow))
				{
					scaleScalar = 1.0f + scaleScalar;
					for (int i = 0; i < _editHandleFlags.Length; ++i)
					{
						if (_editHandleFlags[i])
						{
							_vertices[i].go.transform.localPosition -= scaleCenter;
							_vertices[i].go.transform.localPosition *= scaleScalar;
							_vertices[i].go.transform.localPosition += scaleCenter;
						}
					}
				}
			}
		}

		public void UpdateHandles()
		{
			int numVerts = _numCols * _numRows;
			int prevNumVerts = _vertices.Count;

			// Remove extra vertices.
			while (_vertices.Count > numVerts)
			{
				var vertToRemove = _vertices[_vertices.Count - 1];
				if (vertToRemove.go != null)
				{
#if UNITY_EDITOR
					DestroyImmediate(vertToRemove.go);
#else
					Destroy(vertToRemove.go);
#endif
				}
				vertToRemove.go = null;
				_vertices.RemoveAt(_vertices.Count - 1);
			}

			// Add missing handles.
			while (_vertices.Count < numVerts)
			{
				var vertex = new WarpVertex();
				vertex.go = Instantiate(handlePrefab);
				vertex.go.layer = gameObject.layer;
				vertex.go.transform.parent = transform;
				_vertices.Add(vertex);
			}

			if (_resetVertices || prevNumVerts != _vertices.Count)
			{
				_currHandleIdx = 0;
				_editHandleFlags = new bool[_vertices.Count];

				// Reset vertex positions uniformally on the grid.
				var posOffset = new Vector3(-0.5f, 0.0f, -0.5f);
				var posSlice = new Vector3(1.0f / (NumCols - 1), 1.0f, 1.0f / (NumRows - 1));
				var uvSlice = new Vector2(1.0f / (NumCols - 1), 1.0f / (NumRows - 1));
				for (int row = 0; row < NumRows; ++row)
				{
					for (int col = 0; col < NumCols; ++col)
					{
						int idx = row * NumCols + col;
						_vertices[idx].coord = new Vector2(col, row);
						_vertices[idx].pos = new Vector3(col * posSlice.x + posOffset.x, 0.0f * posSlice.y + posOffset.y, row * posSlice.z + posOffset.z);
						_vertices[idx].uv = new Vector2(col * uvSlice.x, row * uvSlice.y);
						_vertices[idx].go.name = string.Format("Vertex [{0:0}, {1:0}]", col, row);
						_vertices[idx].go.transform.localPosition = _vertices[idx].pos;
						_vertices[idx].go.GetComponent<MeshRenderer>().material.SetColor("_Color", (_editHandleFlags[idx]) ? Color.red : Color.yellow);
					}
				}

				_resetVertices = false;
			}
			else
			{
				// Make sure handles remain on the plane surface.
				for (int row = 0; row < NumRows; ++row)
				{
					for (int col = 0; col < NumCols; ++col)
					{
						int idx = row * NumCols + col;
						_vertices[idx].go.transform.localPosition = new Vector3(_vertices[idx].go.transform.localPosition.x, 0.0f, _vertices[idx].go.transform.localPosition.z);
						_vertices[idx].go.GetComponent<MeshRenderer>().material.SetColor("_Color", (_editHandleFlags[idx]) ? Color.red : Color.yellow);
						_vertices[idx].pos = _vertices[idx].go.transform.localPosition;
					}
				}
			}
		}

		public void RebuildMesh()
		{
			var bounds = new Bounds();
			var positions = new Vector3[NumCols * NumRows];
			var texCoords = new Vector2[NumCols * NumRows];
			for (int row = 0; row < NumRows; ++row)
			{
				for (int col = 0; col < NumCols; ++col)
				{
					int idx = row * NumCols + col;
					positions[idx] = _vertices[idx].pos;
					texCoords[idx] = _vertices[idx].uv;

					bounds.Encapsulate(positions[idx]);
				}
			}

			var indices = new int[(NumCols - 1) * (NumRows - 1) * 2 * 3];
			int currIdx = 0;
			for (int row = 0; row < NumRows - 1; ++row)
			{
				for (int col = 0; col < NumCols - 1; ++col)
				{
					indices[currIdx++] = (col + 0) + (row + 0) * NumCols;
					indices[currIdx++] = (col + 0) + (row + 1) * NumCols;
					indices[currIdx++] = (col + 1) + (row + 0) * NumCols;

					indices[currIdx++] = (col + 1) + (row + 0) * NumCols;
					indices[currIdx++] = (col + 0) + (row + 1) * NumCols;
					indices[currIdx++] = (col + 1) + (row + 1) * NumCols;
				}
			}

			var mesh = new Mesh();
			mesh.vertices = positions;
			mesh.uv = texCoords;
			mesh.bounds = bounds;
			mesh.SetIndices(indices, MeshTopology.Triangles, 0);

			_meshFilter.sharedMesh = mesh;
		}

		public bool SaveData()
		{
			if (string.IsNullOrEmpty(dataFile))
			{
				Debug.LogErrorFormat("[{0}] No data file set!", GetType().Name);
				return false;
			}

			var warpData = new GridWarpData();
			warpData.NumCols = NumCols;
			warpData.NumRows = NumRows;
			warpData.Vertices = new List<VertexData>();
			for (int i = 0; i < Vertices.Count; ++i)
			{
				VertexData vertex = new VertexData();
				vertex.Coord = Vertices[i].coord;
				vertex.Pos = Vertices[i].pos;
				vertex.Uv = Vertices[i].uv;

				warpData.Vertices.Add(vertex);
			}

			var jsonSettings = new JsonSerializerSettings();
			jsonSettings.Formatting = Formatting.Indented;
			jsonSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;

			var jsonData = JsonConvert.SerializeObject(warpData, jsonSettings);
			var dataPath = Path.Combine(Application.streamingAssetsPath, "GridWarp");
			Directory.CreateDirectory(dataPath);
			File.WriteAllText(Path.Combine(dataPath, dataFile), jsonData);

			Debug.LogFormat("[{0}] Data saved successfully to {1}", GetType().Name, dataPath);

			return true;
		}

		public bool LoadData()
		{
			if (string.IsNullOrEmpty(dataFile))
			{
				Debug.LogErrorFormat("[{0}] No data file set!", GetType().Name);
				return false;
			}

			// Load data from file.
			var dataPath = Path.Combine(Path.Combine(Application.streamingAssetsPath, "GridWarp"), dataFile);
			if (!File.Exists(dataPath))
			{
				Debug.LogErrorFormat("[{0}] No file found at {1}!", GetType().Name, dataPath);
				return false;
			}

			var jsonSettings = new JsonSerializerSettings();
			jsonSettings.Formatting = Formatting.Indented;
			jsonSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;

			var jsonData = File.ReadAllText(dataPath);
			var warpData = JsonConvert.DeserializeObject<GridWarpData>(jsonData, jsonSettings);

			ResetVertices();
			_resetVertices = false;

			NumCols = warpData.NumCols;
			NumRows = warpData.NumRows;
			Vertices = new List<WarpVertex>();
			for (int i = 0; i < warpData.Vertices.Count; ++i)
			{
				WarpVertex vertex = new WarpVertex();
				vertex.coord = warpData.Vertices[i].Coord;
				vertex.pos = warpData.Vertices[i].Pos;
				vertex.uv = warpData.Vertices[i].Uv;

				Vertices.Add(vertex);
			}

			if (_editing)
			{
				_editing = false;
				Editing = true;
			}

			RebuildMesh();

			Debug.LogFormat("[{0}] Data loaded successfully from {1}", GetType().Name, dataPath);

			return true;
		}

		public void OnGUI()
		{
			if (_editing)
			{
				var infoStr = "*** Edit Mode: [" + gameObject.name + "]";
				infoStr += "\n  * [TAB] / [SHIFT]+[TAB] to solo / group select next vertex";
				infoStr += "\n  * [BACK] / [SHIFT]+[BACK] to solo / group select prev vertex";
				infoStr += "\n  * [DEL] to clear selection";
				infoStr += "\n  * [W]+[ARROWS] / [SHIFT]+[W]+[ARROWS] to translate step / nudge selection";
				infoStr += "\n  * [E]+[ARROWS] / [SHIFT]+[E]+[ARROWS] to rotate step / nudge selection";
				infoStr += "\n  * [R]+[ARROWS] / [SHIFT]+[R]+[ARROWS] to scale step / nudge selection";
				infoStr += "\n  * [CTRL]+[S] / [CTRL]+[L] to save / load settings";
				infoStr += "\n  * [" + editToggleKey + "] to exit";

				GUI.Box(new Rect(10.0f, 10.0f, 300.0f, 130.0f), "");
				GUI.Label(new Rect(20.0f, 20.0f, 280.0f, 110.0f), infoStr);
			}
		}
	}
}